define(['jquery','underscore','backbone', 'handlebars', 'models/DrawingBlock', 'jsplumb',
        'jsplumbwrap', 'bootstrap', 'jslib', 'evaldrawing' ], 
    function( $ , _ , Backbone, Handlebars, DrawingBlock, jsPlumb, jsWrap, Bootstrap, jslib, EvalDrawing) {
        
        
        var drawingBlockTemplateBase = 
            "<div "+
                "data-cid='{{cid}}' "+
                "parent='drawingblock_{{cid}}' "+
                "data-editable='{{editable}}' "+
                "class='{{sgClass}} icon-item icon-context-menu box menu-1 center-block' "+
                "id=icon_{{name}}_{{cid}} style='left:50px; top:50px;' rel='popover'>";
                
                    
        var editHtml =
                "<div "+
                "class='clearfix pull-right "+
                "action-edit glyphicon glyphicon-pencil'>"+
                "</div>";
        
        var removeHtml = 
                "<div "+
                "class='clearfix pull-right "+
                "action-remove glyphicon glyphicon-remove'>"+
                "</div>"+
                "<div class='clearfix pull-right action-info glyphicon glyphicon-search'></div>";
        
        var endHtml = 
            "<p class='text-center'>{{label}}</p>"+"</div>";
        

        var DrawingBlockView = Backbone.View.extend({
            templateEditable: Handlebars.compile(drawingBlockTemplateBase+editHtml+removeHtml+endHtml),
            
            templateNonEditable: Handlebars.compile(drawingBlockTemplateBase+removeHtml+endHtml),
            
            initialize: function(){
                console.log("DrawingBlockView initialized");
                // _.bindAll(this, 'render', 'renderOne');
                this.model.on('destroy', this.remove);
                this.model.on('change:value', this.renderValue, this);
            },
            
            events: {
                
                "click .action-remove" : "blockRemove",
                "click .action-edit"   : "blockEdit",
                "dblclick"             : "blockEdit",
                "mouseenter .action-info"     : "showBlockInfo",
                "mouseleave .action-info"     : "hideBlockInfo"
            },
            
            renderValue: function() {
                //change value inside drawing block
                var label, value;
                // modify icon label -> if it is a constant (input or output) show the value, 
                // otherwise show the function name
                 var blockType = this.model.get('type');
                if (blockType === 'source' || blockType === 'sink') {
                    var value = parseFloat(this.model.get('value')).toFixed(3);
                    if (isNaN(value)) {
                        label = this.model.get('value');
                    } else {
                        label = value;
                    }
                } else {
                    label = this.model.get('name');
                }
               label = "<p class='text-center'>"+label+"</p>";
                switch (blockType) {
                case 'source':
                    this.$el.find('#'+this.model.get('iconId'))
                        .html(editHtml+removeHtml+label);
                    break;
                default:
                    this.$el.find('#'+this.model.get('iconId'))
                        .html(removeHtml+label);
                    break;
                };
            },
            
            render: function(){

                //check if model has iconId
                if (!this.model.has('iconId')) {
                    //model has no iconId so let's make one
                    console.log("this model does not have an iconId ... will make one");
                    this.model.set(
                        'iconId', 
                        'icon_'+this.model.get('name')+"_"+this.model.cid
                    );
                }
                // console.log('using this model > ');
                // console.log('cid > '+this.model.cid);
                // console.log('editable > '+this.model.get('editable'));
                // console.log('sgClass > '+this.model.get('sgClass'));
                // console.log('operator > '+this.model.get('operator'));
                // console.log('iconId > '+this.model.get('iconId'));
                if (this.model.get('editable')) {
                    //drawing block is editable
                    this.template = this.templateEditable;    
                } else {
                    this.template = this.templateNonEditable;
                }
                
                // modify icon label -> if it is a constant (input or output) show the value, 
                // otherwise show the function name
                if (this.model.get('type') === 'source' || this.model.get('type') === 'sink') {
                    var value = parseFloat(this.model.get('value')).toFixed(3)
                    if (isNaN(value)) {
                        label = this.model.get('value');
                    } else {
                        label = value;
                    }
                } else {
                    label = this.model.get('name');
                }
                this.model.set({label: label});
                // console.log('template is:');
                // console.log(this.template(this.model.toJSON()));
                
                // modify this.el
                $(this.el).attr('id', 'drawingblock_'+this.model.cid);
                var htmlString = this.template(this.model.toJSON());
                $(this.el).html(htmlString);
                return this;
            },
            
            renderWrap: function()  {
                jsWrap.DrawingBlockWrap(
                    this.model.get('iconId'),
                    this.model.get('sgClass'),
                    this.model.get('inputs'),
                    this.model.get('outputs')
                );
                return this;
            },
            
            remove: function() {
                console.log('DrawingBlockView::remove > The following is to be removed');
                console.log(this.model);
                var blockId = '#'+this.get('iconId');
                console.log('blockId = '+blockId);
                console.log('Any parents?');
                console.log($(blockId).parent());
                // delete jsplumb connections to this block
                 jsPlumb.detachAllConnections($(blockId));
                var parent = $(blockId).attr('parent');
                console.log("drawingBlock: " + parent + " will be deleted!");
                $('#'+parent).remove();
                return this;
            },
            blockRemove: function(e) {
                e.preventDefault();
              if (confirm('Are you sure you want to delete this block?')){
                  this.model.destroy({
                      success: function (){
                          console.log(this.model.get('iconId')+" was destroyed");
                      },
                      error: function(err) {
                          console.error(err);
                      }
                  });
                  
                  // drawing has changed -> re-evaluate drawing 
                  EvalDrawing.evaluate();
                  
              }  else {
                  // Do nothing!
                  console.log('user chose not to destroy '+this.model.get('iconId')+' block');
              }
              return this;
            },
            blockEdit: function(e){
                e.preventDefault();
                e.stopPropagation();
                console.log("DrawingBlockView.blockEdit >");
                console.log("current model: ");
                console.log(this.model);
                if (this.model.get('editable')) {
                    // only show dialog for an editable item
                    // var dialogView = new DialogView({model:this.model});
                   // $( "#dialog-form" ).dialog( "open" );
                   var blockValue = prompt("Please enter new value for this block");
                   if (blockValue!=null && blockValue!="") {
                       console.log('replace old value: '+this.model.get('value')+' with new one: '+blockValue);
                       //this.model.set('value', blockValue);
                       this.model.save({value: blockValue});
                   }
                   console.log('this.model.get("value"): ');
                   console.log(this.model.get('value'));
                }
                return this;
            },
            showBlockInfo: function() {
                var blockId = '#'+this.model.get('iconId');
                var blockType = this.model.get('type');
                var content;
                
                switch (blockType) {
                case 'function':
                    var operation = this.model.get('sgClass');
                
                    var inputValues = [];
                    if (this.model.inputBlocks()) {
                        this.model.inputBlocks().forEach(function (input) {
                            inputValues.push(parseFloat(input.get('value')));
                        });
                    }
                    var output = parseFloat(this.model.get('value')).toFixed(3); // only present the first 3 decimal points
                    content = operation + "<br />"+"input values: "+JSON.stringify(inputValues)+
                    "<br />"+"output: "+output;                
                    break;
                case 'sink':
                    // there should be only one input
                    if (this.model.inputBlocks()) {
                        this.model.inputBlocks().forEach(function (input) {
                            content = input.get('value');
                        });
                    }
                    break;
                case 'source':
                    content = this.model.get('value');
                    break;
                };
                
                console.log("content > %r", content);
                $(blockId).popover({
                    selector: '[rel=popover]',
                    title: this.model.get('name'), 
                    content: content, 
                    html:true,
                    trigger: 'hover'
                });
                $(blockId).popover('show');
            },
            hideBlockInfo: function(){
                var blockId = '#'+this.model.get('iconId');
                $(blockId).popover('destroy');
            }
        });
        
        return DrawingBlockView;
        
    });
