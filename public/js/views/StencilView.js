define([
    'jquery',
    'underscore',
    'backbone',
    'marionette',
    'handlebars',
    'views/StencilBlockView'
    ], function($, _, Backbone, Marionette, Handlebars, StencilBlockView) {

        var stencil_library_template = Handlebars.compile(
            // "<div class='btn-group'>"+
            " <button type='button' class='btn btn-primary'>{{name}}</button>"+
            "<button type='button' class='btn btn-primary dropdown-toggle' data-toggle='dropdown'>"+
//            "{{name}}"+
            "<span class='caret'></span>"+
            "</button>"+
            "<ul class='dropdown-menu' role='menu'></ul>"
            // +"</div>"
        );

        var StencilLibraryView = Backbone.Marionette.CompositeView.extend({
           template: stencil_library_template,
   
           className: 'btn-group',
   
           itemView: StencilBlockView,
   
           itemViewContainer: 'ul',
   
           events: {
               //'click a': 'logInfoUrl'
               //'click a': 'addIcon'
           },
   
           initialize: function() {
               this.collection = this.model.get('stencils');
               // console.log('inside LibraryView, icons are %r', JSON.stringify(this.collection));
           },
   
           logInfoUrl: function(){
               console.log(this.model.get('info_url'));
           }
        });
        
        return StencilLibraryView;
        
    });
