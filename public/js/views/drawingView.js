define([
    'jquery',
    'underscore',
    'backbone',
    'bootstrap',
    'handlebars',
    'collections/DrawingCollection',
    'views/DrawingBlockView',
    'jsplumb',
    'views/sourceCodeView',
    'models/SavedDrawing',
    'models/DrawingBlock',
    'tsort',
    'evaldrawing'
    ], function($, _, Backbone, Bootstrap, Handlebars, Drawing, DrawingBlockView, jsPlumb, SourceCodeView, SavedDrawing, DrawingBlock, tsort, EvalDrawing) {

           var DrawingView = Backbone.View.extend({
               el: '#editorButtons', 
               template: Handlebars.compile(
                   "<div drawing-cid='{{cid}}' id='editor'>"+
                   "<button id='save_editor' type='button' class='btn btn-primary pull-right'>Save</button>"+
                   "<button id='load_editor' type='button' class='btn btn-primary pull-right'>Load</button>"+
                   "<button id='code_editor' type='button' class='btn btn-success pull-right'>Code</button>"+
                   "<button id='clear_editor' type='button' class='btn btn-warning pull-right'>Clear</button>"+
                   "</div>"
               ), 
               events: {
                   "click #clear_editor"      : "clearBoard",
                   "click #code_editor"       : "codeBoard",
                   "click #save_editor"       : "saveBoard",
                   "click #load_editor"       : "loadBoard"
               },
              
               initialize: function(){
                   console.log('initialized DrawingView');
                   this.render();
               },
               render: function(){
                   console.log('rendering DrawingView with >');
                   console.log(this.model);
                   var htmlString = this.template(this.model.toJSON());
                   $(this.el).html(htmlString);
                   return this;
               },
               clearBoard: function(){
                   //alert("clicked on clear editor button");
                   console.log('clearBoard() > ');
                   console.log('this.drawingblocks >');
                   console.log(this.model.drawingBlocks);
                   console.log('and it has this many items: '+this.model.drawingBlocks.length);
                   var block;
                   while (block = this.model.drawingBlocks.first()) {
                       block.destroy();
                   }
                   $('#code').html(""); // clear the source code board

                   return this;
               },
               codeBoard: function(){
                   var code = new SourceCodeView({model:this.model});
                   $('#code').append(code.render().el);
               },
               saveBoard: function(){
                   // code to save drawing & drawingModels to database
                   /* Algorithm:
                       1. collect all drawing blocks into JSON representation
                       2. put the drawing blocks as a subdocument of drawing
                       2. put drawing as a subdocument of savedDrawing
                    */
                    console.log("Drawing Blocks to be saved >");
                    console.log(this.model.drawingBlocks);
                    
                    // Add position attributes
                    var blocks = this.model.drawingBlocks;
                    blocks.forEach(function (block) {
                        block.set({
                           left: $('#'+block.get('iconId')).css('left'),
                           top: $('#'+block.get('iconId')).css('top')
                        });
                        console.log(block);
                    });
                    console.log("Blocks should have positions now >");
                    console.log(blocks);
                    
                    // Ask user for to name the save slot
                   var slotName = prompt("Please give a name for this drawing");
                   // first colllect the connections
                   var allConnections = jsPlumb.getAllConnections();
                   var connections=[];
                   
                   // arrange connections into an array
                   allConnections.forEach( function (conn) {
                       // pull out connector uuids for this connection
                       // first is the source
                       // next is the target
                       var connectorUuid=[];
                       conn.endpoints.forEach( function (endpoint) {
                           connectorUuid.push(endpoint.getUuid());
                       });
                       connections.push({id:conn.id,source:connectorUuid[0], target:connectorUuid[1]});
                       
                   });

                   var savedDrawing = new SavedDrawing({
                       drawingBlocks: blocks, 
                       date: new Date, 
                       update: new Date, 
                       name:slotName,
                       connections: connections
                   });
                   console.log("savedDrawing >");
                   console.log(savedDrawing);
                   //console.log("connections collected >");
                   //console.log(conns);
                   //savedDrawing.connections = conns;
                   //console.log(savedDrawing.connections);
                   savedDrawing.save( function (err) {
                       if (err) {
                           console.error(err);
                       }
                   });
                   return this;
                   
               },
               loadBoard: function(){
                   // create an array of saved slots
                   console.log("loadBoard function called");
                   this.clearBoard();
                   
                   $.get('/api/save/drawings', function (savedSlots) {
                       var savedSlotList = "\n";
                       var i = 1;
                       savedSlots.forEach( function (data) {
                           savedSlotList= savedSlotList + i+ ". "+data.name + "\n";
                           i++;
                       });
                       console.log("savedSlotList");
                       console.log(savedSlotList);
                       
                        var loadSelect = prompt("Please choose a drawing to load"+savedSlotList);
                        console.log(savedSlots[parseInt(loadSelect)-1].name);
                    
                        $.get('/api/save/drawings/'+savedSlots[parseInt(loadSelect)-1].name, function (drawing) {
                            console.log("Loading the following saved drawing >");
                            console.log(drawing);
                            // First redraw all the drawingBlocks
                            drawing[0].drawingBlocks.forEach( function (block) {
                                console.log("Block >");
                                console.log(block);
                                console.log("cid >"+block.cid);                            
                                var drawBlock = new DrawingBlock(block);
                                drawBlock.cid = block.cid;
                                drawBlock.set('iconId', 'icon_'+drawBlock.get('name')+'_'+drawBlock.cid);
                                console.log("test >");
                                console.log(drawBlock);
                                console.log("drawBlock.cid >"+drawBlock);
                                var drawingBlock = window.editorDrawing.addDrawingBlock(drawBlock, function (err) {
                                    if (err) {
                                        console.error(err);
                                    }
                                });
                            
                                var drawingBlockView = new DrawingBlockView({model:drawBlock});
                                $('#drawingBoard').append(drawingBlockView.render().el);
                            
                                // add jsplumb layer
                                $('#drawingBoard').append(drawingBlockView.renderWrap().el);
                                
                                // position icon to saved position
                                $('#'+drawBlock.get('iconId')).css({
                                    left:drawBlock.get('left'), 
                                    top: drawBlock.get('top')
                                });

                            });

                            // Now redraw their connections
                            drawing[0].connections.forEach( function (connector) {
                                console.log("Connector >");
                                console.log(connector);
                                jsPlumb.connect({
                                    uuids:[connector.source, connector.target],
                                    editable: true
                                });
                            });
                            // now evaluate the drawing
                            EvalDrawing.evaluate();
                            
                        });
                       
                   });
                   
                   return this;
               }

            });
            
            return DrawingView;
            
        });
