define(['jquery', 'underscore','backbone', 'dot', 'jsplumb',
        'R', 'Python', 'VBA', 'Ruby', 'tsort'], 
function ($, _, Backbone, doT, jsPlumb, R, Python, VBA, Ruby, tsort) {

    var SourceCodeView = Backbone.View.extend({
        el: '#code',
        initialize: function (){
            console.log('SourceCodeView initialized');
            _.bindAll(this, 'render');
        },
        render: function(){

            console.log('SourceCodeView:render received the following model >');
            console.log(this.model);
            
            // get all the connections on the drawing board
            var allConnections = jsPlumb.getAllConnections();
            
            // create connection hash
            var connections = [];
            var edges = [];
            // created block hash
            var blocks = [];

            
            // Build edges data structure
            allConnections.forEach( function(element) {
                edges.push([element.sourceId, element.targetId]);
            });
            
            //console.log("Edges >");
            //console.log(edges);
            
            var sortedBlocks = tsort(edges); // returns an ordered array of iconId
            //console.log("sorted Blocks >");
            //console.log(sortedBlocks);
            
            // now begin building drawing block dictionary for the template
            
            
            // Loop through the sorted drawing blocks to find its connections
            for (i=0; i < sortedBlocks.length; i++) {
                
                // first find connections of this edge
                var inputs = [];
                var outputs = [];
                allConnections.forEach(function (c) {
                    //console.log("working on connection >%s at i=%d",c.id, i);
                    //console.log("looking for the following pair > from: %s, to: %s", edgesSorted[i-1], edgesSorted[i]);
                    //console.log("check against current connection ")
                    
                    // Get input and output connections
                    if (c.targetId === sortedBlocks[i]) {
                        //console.log("Found! input connection > %s", c.id);
                        connections.push(c.id);
                        inputs.push(c.id);
                    } 
                    
                    if (c.sourceId === sortedBlocks[i]) {
                        //console.log("Found! output connection > %s", c.id);
                        outputs.push(c.id);                        
                    }
                    
                });
                
                // now find the drawing block model for this sorted block
                var b = this.model.drawingBlocks.findWhere({iconId: sortedBlocks[i]});
                // make the dictionary
                blocks.push({
                    name:b.get('name'),
                    sgClass:b.get('sgClass'),
                    value: b.get('value'), 
                    type:b.get('type'),
                    inputs: inputs,
                    outputs: outputs 
                });
            }
            
            
            console.log(JSON.stringify(blocks));
            console.log(JSON.stringify(connections));
            
            // Ask user for selection of language
            // var bodyTemplate = something.ask.form;
            var answer = true;
            do 
            {
                
                var languageSelect = prompt("Which Output Language do you want?\nChoices:\n 1. R\n 2. Python\n 3. VBA\n 4. Ruby\n");
                
                switch (languageSelect) {
                  case '1':
                      beginString = R.BeginTemplate;
                      bodyTemplate = R.BodyTemplate;
                      endString = R.EndTemplate;
                      library = R.library;
                      
                      break;
                  case '2':
                      beginString = Python.BeginTemplate;
                      bodyTemplate = Python.BodyTemplate;
                      endString = Python.EndTemplate;
                      library = Python.library;
                      break;
                  case '3':
                      beginString = VBA.BeginTemplate;
                      bodyTemplate = VBA.BodyTemplate;
                      endString = VBA.EndTemplate;
                      library = VBA.library;
                      break;
                  case '4':
                      beginString = Ruby.BeginTemplate;
                      bodyTemplate = Ruby.BodyTemplate;
                      endString = Ruby.EndTemplate;
                      library = Ruby.library;
                      break;
                  case '':
                      beginString = "";
                      bodyTemplate = "";
                      endString = "";
                      library = "";
                      break;
                  default:
                      answer = false; //Invalid answer try again
                };
                
            }
            while (!answer);
            
            
            // prepare doT template
            /*
            console.log("beingString >");
            console.log(beginString);
            console.log("bodyTemplate >");
            console.log(bodyTemplate);
            console.log("endString >");
            console.log(endString);
            */
            var templateFunction = doT.template(bodyTemplate);
            // feed the template with data
            var bodyString = templateFunction({blocks:blocks, connections: connections});
            
            // Create html output
            var htmlString = beginString+bodyString+endString;
            
            console.log('SouceCodeView htmlString');
            console.log(htmlString);
            $('#def').html(library);
            $(this.el).html(htmlString);
            return this;
        }
    });
    
    return SourceCodeView;
});