
define(['backbone', 'marionette', 'handlebars', 'jsplumbwrap',
'models/DrawingBlock','views/DrawingBlockView', 'evaldrawing'], 
    function (Backbone, Marionette, Handlebars, block, DrawingBlock, DrawingBlockView, EvalDrawing ) {                    
                    
        var stencil_template = Handlebars.compile(
            //"<a class='icon_item {{sgClass}}' href='#icon_{{id}}'>{{name}}</a>");
            "<a class='icon_item {{sgClass}}' href='#'>{{name}}</a>");
            
        var StencilBlockView = Backbone.Marionette.ItemView.extend({
            template: stencil_template,
            tagName: 'li',

            events: {
                'click a': 'addDrawingBlock'
            },

            addDrawingBlock: function () {
                // for debugging ease
                var functionName = "StencilBlockView::addDrawingBlock > ";

                // copy the stencil model basic attributes 
                // and create a new drawingBlock based with those attributes
                var drawingBlock = new DrawingBlock();                
                drawingBlock.set({
                    name:        this.model.get('name'),
                    description: this.model.get('description'),
                    editable:    this.model.get('editable'),
                    value:       this.model.get('value'),
                    sgClass:     this.model.get('sgClass'),
                    type:        this.model.get('type'),
                    inputs:      this.model.get('inputs'),
                    outputs:     this.model.get('outputs'),
                    operation:   this.model.get('operation')
                });
                
                console.log(functionName+"StencilBlockView.js line[31] drawingBlock >");
                console.log(drawingBlock);
                
                //check that we do have a different cid for both models
                if (this.model.cid == drawingBlock.cid) {
                    console.error('Houston we have a clone!');
                    console.error('this.model.cid > ' + this.model.cid);
                    console.error('drawingBlock.cid >' + drawingBlock.cid);
                } else {
                    console.log('All is well, moving on ...');
                }
        
                var editorDrawing = window.editorDrawing; // for ease of handling
                console.log("Do we have a drawing model?");
                if (editorDrawing) {
                    console.log("Yes, we do >");
                    console.log(editorDrawing);
                } else {
                    console.log("No we don't!");
                }
        
                var drawingBlock = editorDrawing.addDrawingBlock(drawingBlock, function(err){
                    if (!err) { 
                        console.log("added drawing block to drawing");
                    } else { 
                        console.error(err);
                    }
                });
        
                // add the icon onto the editor drawing board
                var drawingBlockView = new DrawingBlockView({model:drawingBlock});
                $('#drawingBoard').append(drawingBlockView.render().el);
                $('#drawingBoard').append(drawingBlockView.renderWrap().el);
                
                //now revaluate the drawing 
                EvalDrawing.evaluate();
            }
        });
        
        return StencilBlockView;
    });
