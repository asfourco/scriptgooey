define([
    'underscore',
    'backbone'
    ], function( _, Backbone) {

        // Drawing icons on editor board

        var DrawingBlock = Backbone.Model.extend({
            toJSON: function() {
              var json = Backbone.Model.prototype.toJSON.apply(this, arguments);
              json.cid = this.cid;
              return json;
            },
             parse: function( res ) {
                 res.id = res._id;
                 return res;
             },
             inputBlocks: function (){
                 // return name of block(s) that are connected to this block
                 var iconId = this.get('iconId');
                 var drawingBlocks = this.collection.drawing.drawingBlocks;
                 if (this.get('inputs') > 0) {
                     var allConnections = jsPlumb.getAllConnections();
                     var connections = [];
                     var blocks = [];
                     allConnections.forEach( function (connector) {
                         if (connector.targetId === iconId) {
                             connections.push(connector);
                             blocks.push(drawingBlocks.findWhere({iconId: connector.sourceId}));
                         }
                     });
                     if (connections.length > 0 && blocks.length > 0) {
                         console.log("Found %d incoming connections and %d blocks", connections.length, blocks.length);
                         return blocks;
                     } else {
                         console.log("Did not find any inputs for this model [cid:%s]", this.cid)
                     }
                 } else {
                     console.error("This model cid:%s should not have inputs", this.cid);
                 }
                 
             },
             outputOfBlock: function(){
                 // return output of this block
                 // if this block is a function, then the result of the function if the inputs are valid
                 // otherwise return the value of the block
             }
             /*,
             url: function() {
                 this.collection.url() + '/' + this.get('id');
             }*/
        });
        
        return DrawingBlock;
});
