define([
    'underscore',
    'backbone',
    'models/Drawing',
    ], function ( _, Backbone, Drawing) {

        var SavedDrawing = Backbone.Model.extend({
            url: function() {
//                return '/api/save/drawings/' + this.get('id');
                return '/api/save/drawings';
            },
            parse: function (res) {
                res.id = res._id;
                return res;
            },
            initialize: function(drawing) {
                this.drawing = drawing;
                console.log("Initialized SavedDrawing Model");
                console.log("SavedDrawing: this >");
                console.log(this);
            },
            addDrawing: function (drawing) {
                console.log("Trying to addDrawing with: ");
                console.log(drawing);
                var saveDrawing = this.drawing.create(drawing);
                return saveDrawing;
            },
            toJSON: function () {
                var json = Backbone.Model.prototype.toJSON.apply(this, arguments);
                json.cid = this.cid;
                return json;
            }
        });

        return SavedDrawing;
    });

