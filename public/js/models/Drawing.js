define([
    'underscore',
    'backbone',
    'collections/DrawingBlockCollection'
    ], function(_, Backbone, DrawingBlocks) {

            var Drawing = Backbone.Model.extend({
                url: function () {
                  return '/api/drawings/' + this.get('name');  
                },
                initialize: function () {
                    this.drawingBlocks = new DrawingBlocks([], {drawing: this});
                    console.log("Initialized Drawing Model");
                    console.log("drawing: this >");
                    console.log(this);
                    console.log("this.drawingBlocks >");
                    console.log(this.drawingBlocks);
                    console.log("check that we have a url >");
                    console.log(this.url());
                },
                addDrawingBlock: function (drawingBlock) {
                    console.log("trying to addDrawIcon with :");
                    console.log(drawingBlock);
                    var block = this.drawingBlocks.create(drawingBlock);
                    console.log("this.drawingBlocks.length: "+this.drawingBlocks.length);
                    console.log("this.drawingBlocks >");
                    console.log(this.drawingBlocks);
                    return block;
                },
                toJSON: function() {
                  var json = Backbone.Model.prototype.toJSON.apply(this, arguments);
                  json.cid = this.cid;
                  return json;
                }
            });
            
            return Drawing;
});
    
