define([
    'underscore',
    'backbone',
    'models/SavedDrawing',
    'models/Drawing',
    ], function( _, Backbone, SavedDrawing, Drawing) {
        
        // Collection of Saved Drawings
        var SavedDrawings = Backbone.Collection.extend({
            model: SavedDrawing,
            url: '/api/save/drawing',
            initialize: function () {
                console.log("Initialized Saved Drawing Collection");
                this.listenTo(this, 'reset', this.getDrawing);
                this.listenTo(this, 'change', this.getDrawing);
            },
            addDrawing: function() {
                this.collection.create(new SavedDrawing());
            },
            getDrawing: function() {
                this.each(function (savedDrawing) {
                    savedDrawing.drawing = new Drawing([], { drawing: savedDrawing });
                    savedDrawing.drawing.fetch();
                    console.log("Saved >");
                    console.log(JSON.stringify(savedDrawing.drawing.fetch()));
                });
            }
        });

        return SavedDrawings;
    });
