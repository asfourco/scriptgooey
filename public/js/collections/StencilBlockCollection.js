define([
    'underscore',
    'backbone',
    'models/StencilBlock'
    ], function(_, Backbone, StencilBlock) {

        var StencilBlocks = Backbone.Collection.extend({
            model: StencilBlock
        });
        
        return StencilBlocks;
    });