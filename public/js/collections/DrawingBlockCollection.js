define([
    'underscore',
    'backbone',
    'models/DrawingBlock'
    ], function(_, Backbone, DrawingBlock) {

        // This are the drawing items on the editor board
        var DrawingBlocks = Backbone.Collection.extend({
            model: DrawingBlock,
            initialize: function (models, options) {
                this.drawing = options.drawing;
            },
            url: function() {
                // check that we have a url coming in
                /*
                if (this.drawing.url().isNaN) {
                    console.error('NO drawing url passed through');
                } else {
                    console.log('DrawingBlocks received the following url >');
                    console.log(this.drawing.url());
                    
                }
                */
                return this.drawing.url() + '/drawingblocks';
            }
        });
        
        return DrawingBlocks;
    });