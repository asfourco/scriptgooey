define([
    'underscore',
    'backbone',
    'models/Stencil'
    ], function(_, Backbone, Stencil) {

        var Stencils = Backbone.Collection.extend({
            model: Stencil,
            url:'/api/stencils'
        });
        
        return Stencils;
    });