define([
    'underscore',
    'backbone',
    'models/StencilBlock',
    'models/Drawing'
    ], function(_, Backbone, StencilBlock, Drawing) {

        // Collection of drawings ... think of these as documents
        var Drawings = Backbone.Collection.extend({
            model: Drawing,
            url: '/api/drawings',
            initialize: function () {
                console.log("Initialized Drawing Collection");
        
                this.listenTo(this, 'reset', this.getDrawingBlocks);
                
                this.listenTo(this, 'change', this.getDrawingBlocks);
            },
            addDrawing: function() {
                this.collection.create(new Drawing());
            },
            getDrawingBlocks: function () {
                this.each(function (drawing) {
                    drawing.drawingBlocks = new DrawingBlock([], { drawing: drawing });
                    drawing.drawingBlocks.fetch();
                    console.log("our fetch >");
                    console.log(JSON.stringify(drawing.drawingBlocks.fetch()));
                });
            }
        });
        
        return Drawings;
    });
