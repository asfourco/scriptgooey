define(function () {
    
    var beginString = 'Private Sub main()<br />';
            
    // Template really is for the class SG_cos/sin/tan
    var bodyTemplate = 
            // define variable block
            '{{~ it.connections :a }}'+
            '    Dim {{=a}} as Double<br>'+
            '{{~}}'+
            '<br>'+
            // assign variable block
            '{{~ it.blocks :b }}'+
                '{{? b.type === "source"}}'+
                '    {{=b.outputs}} = {{=b.value}}<br>'+
                '{{?}}'+
            '{{~}}'+
            '<br>'+
            // evaluation block
            '{{~ it.blocks :c }}'+
                '{{? c.type === "function"}}'+
                    '    {{= c.outputs}} = {{=c.sgClass}}({{=c.inputs}})<br>'+
                '{{?}}'+
            '{{~}}'+
            '<br>';
            
    var endString = 'End Sub';
    
    
    var library = " \
        Imports System.Math <br /> \
        <br /> \
        Public Function SG_Cos(ByVal a As Double) As Double <br /> \
            Dim angle as Double <br /> \
            angle = (22 / 7) * a / 180.0 <br /> \
            SG_Cos = Math.Cos(angle) <br /> \
        End Function <br /> \
        <br /> \
        Public Function SG_Sin(ByVal a As Double) As Double <br /> \
            Dim angle as Double <br /> \
            angle = (22 / 7) * a / 180.0 <br /> \
            SG_Sin = Math.Sin(a) <br /> \
        End Function <br /> \
        <br /> \
        Public Function SG_Tan(ByVal a As Double) As Double <br /> \
            Dim angle as Double <br /> \
            angle = (22 / 7) * a / 180.0 <br /> \
            SG_Tan = Math.Tan(a) <br /> \
        End Function <br /> \
        <br /> \
        Public Function SG_Addition(ByVal a As Double, ByVal b As Double) As Double <br /> \
            SG_Addition = a + b <br /> \
        End Function <br /> \
        <br /> \
        Public Function SG_Subtraction(ByVal a As Double, ByVal b As Double) As Double <br /> \
            SG_Subtraction = a - b <br /> \
        End Function <br /> \
        <br /> \
        Public Function SG_Division(ByVal a As Double, ByVal b As Double) As Double <br /> \
            SG_Division = a / b <br /> \
        End Function <br /> \
        <br /> \
        Public Function SG_Multiplication(ByVal a As Double, ByVal b As Double) As Double <br /> \
            SG_Multiplication = a * b <br /> \
        End Function <br /> \
        <br /> \
        Public Function SG_Inverse(ByVal a As Double) As Double <br /> \
            SG_Inverse = 1/a <br /> \
        End Function <br /> \
        <br /> \
        Public Function SG_Square_root(ByVal a As Double) As Double <br /> \
            SG_Square_root = Sqr(a) <br /> \
        End Function <br /> \
        <br /> \
        Public Function SG_Square(ByVal a As Double) As Double <br /> \
            SG_Square = a * a <br /> \
        End Function <br /> \
        <br /> \
        Public Function SG_Cube(ByVal a As Double) As Double <br /> \
            SG_Cube = a * a * a <br /> \
        End Function <br /> \
        <br /> \
        Public Function SG_Ln(ByVal a As Double) As Double <br /> \
            SG_Ln = Math.Log(a) <br /> \
        End Function <br /> \
        <br /> \
        Public Function SG_Log(ByVal a As Double) As Double <br /> \
            SG_Log = Math.Log(a, 10) <br /> \
        End Function <br /> \
        <br /> \
    ";
    
    
    return {
      BeginTemplate: beginString,
      BodyTemplate: bodyTemplate,
      EndTemplate: endString,
      library: library
    };
    
});