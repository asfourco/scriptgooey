define(function () {
    
    var beginString = '# Python Script <br />';
            
    // Template really is for the class SG_cos/sin/tan
    var bodyTemplate = 
            // define variable block
            // '{{~ it.connections :a }}'+
            //         'Dim {{=a}} as Integer<br>'+
            // '{{~}}'+
            // '<br>'+
            // no need to define variables in python
            
            // assign variable block
            '{{~ it.blocks :b }}'+
                '{{? b.type === "source"}}'+
                    '    {{=b.outputs}} = {{=b.value}}<br>'+
                '{{?}}'+
            '{{~}}'+
            '<br>'+
            // evaluation block
            '{{~ it.blocks :c }}'+
                '{{? c.type === "function"}}'+
                    '    {{= c.outputs}} = {{=c.sgClass}}({{=c.inputs}})<br>'+
                '{{?}}'+
            '{{~}}'+
            '<br>';
            
    var endString = '';
    
    var library = " \
    import math
    
    def SG_Cos(a): <br /> \
        return math.cos(a) <br /> \
         <br /> \
    def SG_Sin(a): <br /> \
        return math.sin(a) <br /> \
         <br /> \
    def SG_Tan(a): <br /> \
        return math.tan(a) <br /> \
         <br /> \
    def SG_Addition(a,b): <br /> \
        return a+b <br /> \
         <br /> \
    def SG_Subtraction(a,b): <br /> \
        return a - b <br /> \
         <br /> \
    def SG_Division(a,b): <br /> \
        return a / b <br /> \
         <br /> \
    def SG_Multiplication(a,b): <br /> \
        return a * b <br /> \
         <br /> \
    def SG_Square_root(a): <br /> \
        return math.sqrt(a) <br /> \
         <br /> \
    def SG_Square(a): <br /> \
        return a * a <br /> \
         <br /> \
    def SG_Cube(a): <br /> \
        return a * a * a <br /> \
         <br /> \
    def SG_Inverse(a): <br /> \
        return 1.0/a <br /> \
         <br /> \
    def SG_Log(a): <br /> \
        return math.log10(a) <br /> \
         <br /> \
    def SG_Ln(a): <br /> \
        return math.log(a) <br /> \
    ";
    
    return {
      BeginTemplate: beginString,
      BodyTemplate: bodyTemplate,
      EndTemplate: endString,
      library: library
    };
});