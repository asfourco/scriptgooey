var jslib = {};

jslib.SG_Cos = function (inputValues) {
    return Math.cos(inputValues[0]);
};   

jslib.SG_Sin = function (inputValues) {   
    return Math.sin(inputValues[0]);     
};             

jslib.SG_Tan = function (inputValues) {   
    return Math.tan(inputValues[0]);      
};             

jslib.SG_Addition = function (inputValues) { 
    var a = inputValues[0];
    var b = inputValues[1];
    return a + b;       
};             

jslib.SG_Subtraction = function (inputValues) { 
    var a = inputValues[0];
    var b = inputValues[1];
    return a - b;       
};             

jslib.SG_Division = function (inputValues) {
    var a = inputValues[0];
    var b = inputValues[1];
    return a / b;       
};             

jslib.SG_Multiplication = function (inputValues) {
    var a = inputValues[0];
    var b = inputValues[1];
    return a * b;       
};             

jslib.SG_Square = function (inputValues) { 
    var a = inputValues[0];  
    return a * a;
};             

jslib.SG_Cube = function (inputValues) { 
    var a = inputValues[0]; 
    return a * a * a;
};             

jslib.SG_SquareRoot = function (inputValues) {   
    return Math.sqrt(inputValues[0]);     
};             

jslib.SG_Inverse = function (inputValues) {
    var a = inputValues[0];
    return 1 / a;       
};             

jslib.SG_Log = function (inputValues) {   
    return Math.log10(inputValues[0]);
};             

jslib.SG_Ln = function (inputValues) {    
    return Math.log(inputValues[0]);
};             

// for node.js
if (typeof exports == 'object' && exports === this) {
  module.exports = jslib;
}
