define(function () {
    
    var beginString = '#! /usr/bin/env Rscript<br />';
            
    // Template really is for the class SG_cos/sin/tan
    var bodyTemplate = 
            // define variable block
            // '{{~ it.connections :a }}'+
            //         'Dim {{=a}} as Integer<br>'+
            // '{{~}}'+
            // '<br>'+
            // no need to define variables in R
            
            // assign variable block
            '{{~ it.blocks :b }}'+
                '{{? b.type === "source"}}'+
                    '    {{=b.outputs}} <- {{=b.value}}<br>'+
                '{{?}}'+
            '{{~}}'+
            '<br>'+
            // evaluation block
            '{{~ it.blocks :c }}'+
                '{{? c.type === "function"}}'+
                    '    {{= c.outputs}} <- {{=c.sgClass}}({{=c.inputs}})<br>'+
                '{{?}}'+
            '{{~}}'+
            '<br>';
            
    var endString = '';
    
    var library = " \
    SG_Cos <- function(a) { <br /> \
 		result <- cos(a)<br /> \
		result<br /> \
	} <br /> \
	<br /> \
    SG_Sin <- function(a) { <br /> \
 		result <- sin(a)<br /> \
		result<br /> \
	} <br /> \
	<br /> \
    SG_Tan <- function(a) { <br /> \
 		result <- tan(a)<br /> \
		result<br /> \
	} <br /> \
	<br /> \
    SG_Addition <- function(a,b) { <br /> \
 		result <- a + b <br /> \
		result<br /> \
	} <br /> \
	<br /> \
    SG_Subtraction <- function(a,b) { <br /> \
 		result <- a - b<br /> \
		result<br /> \
	} <br /> \
	<br /> \
    SG_Division <- function(a,b) { <br /> \
 		result <- a / b <br /> \
		result<br /> \
	} <br /> \
	<br /> \
    SG_Multiplication <- function(a,b) { <br /> \
 		result <- a * b<br /> \
		result<br /> \
	} <br /> \
	<br /> \
    SG_Square_root <- function(a) { <br /> \
 		result <- sqrt(a)<br /> \
		result<br /> \
	} <br /> \
	<br /> \
    SG_Square <- function(a) { <br /> \
 		result <- a * a<br /> \
		result<br /> \
	} <br /> \
	<br /> \
    SG_Cube <- function(a) { <br /> \
 		result <- a * a * a<br /> \
		result<br /> \
	} <br /> \
	<br /> \
    SG_Inverse <- function(a) { <br /> \
 		result <- 1 / a<br /> \
		result<br /> \
	} <br /> \
	<br /> \
    SG_Log <- function(a) { <br /> \
 		result <- log10(a)<br /> \
		result<br /> \
	} <br /> \
	<br /> \
    SG_Ln <- function(a) { <br> \
        result <- log2(a)<br /> \
        result <br /> \
    } <br />";
    
    
    return {
      BeginTemplate: beginString,
      BodyTemplate: bodyTemplate,
      EndTemplate: endString,
      library: library
    };
    
});