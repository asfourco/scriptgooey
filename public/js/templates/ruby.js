define(function () {
    
    var beginString = '#!/usr/bin/ruby <br /><br />';
            
    // Template really is for the class SG_cos/sin/tan
    var bodyTemplate = 
            // define variable block
            // '{{~ it.connections :a }}'+
            //         'Dim {{=a}} as Integer<br>'+
            // '{{~}}'+
            // '<br>'+
            // no need to define variables in ruby
            
            // assign variable block
            '{{~ it.blocks :b }}'+
                '{{? b.type === "source"}}'+
            '    {{=b.outputs}} = {{=b.value}}<br>'+
                '{{?}}'+
            '{{~}}'+
            '<br>'+
            // evaluation block
            '{{~ it.blocks :c }}'+
                '{{? c.type === "function"}}'+
            '    {{= c.outputs}} = {{=c.sgClass}}({{=c.inputs}})<br>'+
                '{{?}}'+
            '{{~}}'+
            '<br>';
            
    var endString = '';

    var library = "\
        def SG_Cos(a)   <br /> \
            Math.cos(a)      <br /> \
        end             <br /> \
        <br /> \
        def SG_Sin(a)   <br /> \
            Math.sin(a)      <br /> \
        end             <br /> \
        <br /> \
        def SG_Tan(a)   <br /> \
            Math.tan(a)      <br /> \
        end             <br /> \
        <br /> \
        def SG_Addition(a,b)    <br /> \
            a + b       <br /> \
        end             <br /> \
        <br /> \
        def SG_Subtraction(a,b) <br /> \
            a - b       <br /> \
        end             <br /> \
        <br /> \
        def SG_Division(a,b)    <br /> \
            a / b       <br /> \
        end             <br /> \
        <br /> \
        def SG_Multiplication(a,b)  <br /> \
            a * b       <br /> \
        end             <br /> \
        <br /> \
        def SG_Square(a)    <br /> \
            a * a       <br /> \
        end             <br /> \
        <br /> \
        def SG_Cube(a)  <br /> \
            a * a * a   <br /> \
        end             <br /> \
        <br /> \
        def SG_Square_root(a)   <br /> \
            Math.sqrt(a)     <br /> \
        end             <br /> \
        <br /> \
        def SG_Inverse(a)   <br /> \
            1.0 / a       <br /> \
        end             <br /> \
        <br /> \
        def SG_Log(a)   <br /> \
            Math.log10(a)    <br /> \
        end             <br /> \
        <br /> \
        def SG_Ln(a)    <br /> \
            Math.log(a)      <br /> \
        end             <br /> \
    ";
    
    return {
      BeginTemplate: beginString,
      BodyTemplate: bodyTemplate,
      EndTemplate: endString,
      library: library
    };
});