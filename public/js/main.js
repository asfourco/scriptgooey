
// define requirejs
requirejs.config({
    baseUrl: "/js",
    paths: {
        app: 'app',
        collections: 'collections',
        lib: 'lib',
        models: 'models',
        views: 'views',
        templates: 'templates',
        R: 'templates/r-project',
        Python: 'templates/python',
        Ruby: 'templates/ruby',
        VBA: 'templates/vba',
        jslib: 'templates/javascript',
        jsplumbenv: 'app/jsplumbenv',
        jsplumbwrap: 'app/drawingblockwrap',
        jquery: 'lib/jquery-2.0.3.min',
        jsplumb: 'lib/jquery.jsPlumb-1.5.3',
        jqueryui:  'lib/jquery-ui-1.10.3.custom',
        jquerytouch: 'lib/jquery.ui.touch-punch',
        underscore: 'lib/underscore',
        backbone:    'lib/backbone',
        marionette: 'lib/backbone.marionette',
        handlebars: 'lib/handlebars',
        bootstrap: 'lib/bootstrap',
        dot: 'lib/doT',
        tsort: 'app/tsort',
        evaldrawing: 'app/evaldrawing'
    },
    shim:{
        'jqueryui': {
            deps:['jquery'],
        },
        'jquerytouch': {
            deps: ['jquery']
        },
        'lib/jquery.ui.position':{
            deps:['jquery', 'jqueryui']
        },
        'lib/jquery.contextMenu':{
            deps:['jquery']
        },
        'jsplumb': {
            deps:['jquery']
        },
        'backbone': {
            deps: ['underscore', 'jquery'],
            exports: 'Backbone'
        },
        'marionette':{
            deps: ['jquery','backbone'],
            exports: 'Marionette'
        },
        'underscore': {
            exports: '_'
        },
        'bootstrap':{
            deps:['jquery'],
            exports:'Bootstrap'
        },
        'handlebars':{
            exports: 'Handlebars'
        },
        'jsplumbenv':{
            deps: ['jquery', 'jsplumb', 'lib/jquery.ui.position', 'jqueryui'],
        },
        'jsplumbwrap':{
            deps: ['jsplumb'],
            exports: 'jsWrap'
        },
        'dot':{
            exports: 'doT'
        },
        'R': {
            exports: 'R'
        },
        'Python': {
            exports: 'Python'
        },
        'VBA': {
            exports: 'VBA'
        },
        'Ruby': {
            exports: 'Ruby'
        },
        'jslib': {
            exports: 'jslib'
        },
        'tsort': {
            exports: 'tsort'
        },
        'evaldrawing': {
            exports: 'EvalDrawing'
        }
        
    }
    
});
// start
require([
        'jsplumb', 
        'app/app',
        'app/index',
        'jsplumbenv',
        'tsort',
        'evaldrawing'
        //'bootstrap'
        ], 
        function() { 
            console.log("bootstrap js loaded successfully");
        }
);
