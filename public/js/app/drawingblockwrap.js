define(function() {
        
        var jsPlumb = require('jsplumb');
        
        return {
            DrawingBlockWrap: function(iconId, sgClass, inputs, outputs) {
                            console.log("the following parameters where passed in:");
                            console.log("iconId: " + iconId);
                            console.log("inputs: " + inputs);
                            console.log("outputs: " + outputs);

                            // limit the draggable class to the editor box
                            jsPlumb.draggable($("." + sgClass), {
                                containment: '#editorBoard'
                            });

                            console.log("jsPlumb.draggable called");

                            var inputConnectorString = [];
                            var outputConnectorString = [];

                            // construct the input endpoints
                            switch (inputs) {
                            case 0:
                                inputConnectorString = [];
                                break;
                            case 1:
                                inputConnectorString = ["Left"];
                                break;
                            case 2:
                                inputConnectorString = ["TopLeft", "BottomLeft"];
                                break;
                            case 3:
                                inputConnectorString = ["TopLeft", "MiddleLeft", "BottomLeft"];
                                break;
                            };

                            // construct the output endpoints
                            switch (outputs) {
                            case 0:
                                outputConnectorString = [];
                                break;
                            case 1:
                                outputConnectorString = ["Right"];
                                break;
                            case 2:
                                outputConnectorString = ["TopLeft", "BottomLeft"];
                                break;
                            case 3:
                                outputConnectorString = ["TopLeft", "MiddleLeft", "BottomLeft"];
                                break;
                            };
                            // make sure all is well
                            console.log( 
                                    "switch statements passed: output = %r, input = %r", 
                                    outputConnectorString,
                                    inputConnectorString
                                );

                            // add the endpoint overlays on the drawicon    
                            jsPlumb.addEndpoints(iconId, outputConnectorString, inputConnectorString);
                            console.log("leaving jsPlumbWrap");
                            
                            // add outputConnectorString and inputConnectorString to model
                            return this;
                        }
        }
});
