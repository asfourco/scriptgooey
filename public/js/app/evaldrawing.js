// FIXME: require.js belches at loading this module
//define(['tsort'],function (tsort) {
define(function () {
    var tsort = require('tsort');
    
    return {
        evaluate: function () {
            var drawing = window.editorDrawing;
            console.log("called evalDrawing function with this object > %r", drawing);
       
            // get all the connections on the drawing board
            var allConnections = jsPlumb.getAllConnections();
    
            // create connection hash
            var connections = [];
            var edges = [];
            // created block hash
            var blocks = [];

    
            // Build edges data structure
            allConnections.forEach( function(element) {
                edges.push([element.sourceId, element.targetId]);
            });
    
            console.log("Edges >");
            console.log(edges);
    
            var sortedBlocks = tsort(edges); // returns an ordered array of iconId
            console.log("sorted Blocks >");
            console.log(sortedBlocks);
    
            // now begin building drawing block dictionary for the template
    
    
            // Loop through the sorted drawing blocks to find its connections
            for (i=0; i < sortedBlocks.length; i++) {
        
                // first find connections of this edge
                var inputs = [];
                var outputs = [];
                allConnections.forEach(function (c) {
                    //console.log("working on connection >%s at i=%d",c.id, i);
                    //console.log("looking for the following pair > from: %s, to: %s", edgesSorted[i-1], edgesSorted[i]);
                    //console.log("check against current connection ")
            
                    // Get input and output connections
                    if (c.targetId === sortedBlocks[i]) {
                        //console.log("Found! input connection > %s", c.id);
                        connections.push(c.id);
                        inputs.push(c.id);
                    } 
            
                    if (c.sourceId === sortedBlocks[i]) {
                        //console.log("Found! output connection > %s", c.id);
                        outputs.push(c.id);                        
                    }
            
                });
        
                // now find the drawing block model for this sorted block
                console.log("working on iconId > %s", sortedBlocks[i]);
                var b = drawing.drawingBlocks.findWhere({iconId: sortedBlocks[i]});
                var blockType = b.get('type');
                console.log("blockType > %s", blockType);
        
                switch (blockType) {
                case 'function':
                    var operation = b.get('sgClass');
                    // find inputs
                    var inputValues = [];
                    if (b.inputBlocks().length){
                        b.inputBlocks().forEach(function (input) {
                            inputValues.push(parseFloat(input.get('value')));
                        });
                        // set the model value
                        b.save({value: jslib[operation](inputValues)});
                    } 
                    break;
                    
                case 'sink':
                    // there should be only one input
                    if (b.inputBlocks().length){
                        b.inputBlocks().forEach(function (input) {
                            b.save({value: input.get('value')});
                        });
                    }
                    break;
                };
                console.log("block value is now > %r", b.get('value'));
            } // <- for loop
            
            return drawing;
        } // <- function 
    } // <- return 

});

// // for node.js
// if (typeof exports == 'object' && exports === this) {
//   module.exports = evalDrawing;
// }

