// Setup jsPlumb environment
define([
    'jsplumb', 'evaldrawing'
    ], function(jsPlumb, EvalDrawing) {

        jsPlumb.ready(function() {
            console.log("jsPlumbInstance is called");

            jsPlumb.importDefaults({
                // default drag options
                DragOptions: {
                    cursor: 'pointer',
                    zIndex: 2000
                },
                // default to blue at one end and green at the other
                EndpointStyles: [{
                    fillStyle: '#225588'
                }, {
                    fillStyle: '#558822'
                }],
                // blue endpoints 7 px; green endpoints 11.
                Endpoints: [
                    ["Dot", {
                        radius: 7
                    }],
                    ["Dot", {
                        radius: 11
                    }]
                ],
                // the overlays to decorate each connection with.  note that the label overlay uses a function to generate the label text; in this
                // case it returns the 'labelText' member that we set on each connection in the 'init' method below.
                ConnectionOverlays: [
                    ["Arrow", {
                        location: 1
                    }],
//                    ["Label", {
//                        location: 0.4,
//                        id: "label",
//                        cssClass: "aLabel"
//                    }]

                ]
               
            });
            var connectorPaintStyle = {
                lineWidth: 4,
                strokeStyle: "#deea18",
                joinstyle: "round",
                outlineColor: "#EAEDEF",
                outlineWidth: 2
            };
            // .. and this is the hover style. 
            var connectorHoverStyle = {
				lineWidth:4,
				strokeStyle:"#5C96BC",
				outlineWidth:2,
				outlineColor:"white"
            };
            var endpointHoverStyle = {
                fillStyle:"#5C96BC"
            };
            // the definition of source endpoints (the small blue ones)
            var sourceEndpoint = {
                endpoint: "Dot",
                paintStyle: {
                    strokeStyle: "#225588",
                    fillStyle: "transparent",
                    radius: 7,
                    lineWidth: 2
                },
                isSource: true,
                connector: ["Flowchart", {
                    stub: [15,25],
                    gap: 10,
                    cornerRadius: 5,
                    alwaysRespectStubs: true
                }],
                connectorStyle: connectorPaintStyle,
                hoverPaintStyle: endpointHoverStyle,
                connectorHoverStyle: connectorHoverStyle,
                dragOptions: {},
                maxConnections: -1,
                overlays: [
                    ["Label", {
                        location: [0.5, 1.5],
                        label: "Drag",
                        cssClass: "endpointSourceLabel"
                    }]
                ]
            };
            // the definition of target endpoints (will appear when the user drags a connection) 
            var targetEndpoint = {
                endpoint: "Dot",
                paintStyle: {
                    fillStyle: "#558822",
                    radius: 11
                },
                hoverPaintStyle: endpointHoverStyle,
                maxConnections: 1,
                dropOptions: {
                    hoverClass: "hover",
                    activeClass: "activeBlock"
                },
                isTarget: true,
                overlays: [
                    ["Label", {
                        location: [0.5, - 0.5],
                        label: "Drop",
                        cssClass: "endpointTargetLabel"
                    }]
                ]
            };
            
            var init = function(connection) {
                    connection.getOverlay("label").setLabel(connection.sourceId.slice(-3) + "-" + connection.targetId.slice(-3));
                        // .setLabel(connection.sourceId.substring(6) + "-" + connection.targetId.substring(6));
                        
                    connection.bind("editCompleted", function(o) {
                        if (typeof console != "undefined") console.log("connection edited. path is now ", o.path);
                    });
            };

            var allSourceEndpoints = [],
                allTargetEndpoints = [];
            jsPlumb.addEndpoints = function(toId, sourceAnchors, targetAnchors) {
                for (var i = 0; i < sourceAnchors.length; i++) {
                    var sourceUUID = toId + sourceAnchors[i];
                    allSourceEndpoints.push(jsPlumb.addEndpoint(toId, sourceEndpoint, {
                        anchor: sourceAnchors[i],
                        uuid: sourceUUID
                    }));
                }
                for (var j = 0; j < targetAnchors.length; j++) {
                    var targetUUID = toId + targetAnchors[j];
                    allTargetEndpoints.push(jsPlumb.addEndpoint(toId, targetEndpoint, {
                        anchor: targetAnchors[j],
                        uuid: targetUUID
                    }));
                }
            };
            
            jsPlumb.bind("connection", function(connInfo, originalEvent) {
                init(connInfo.connection);
            });
            jsPlumb.bind("click", function(conn, originalEvent) {
                if (confirm("Delete connection from " + conn.sourceId + " to " + conn.targetId + "?")) {
                    jsPlumb.detach(conn); 
                    EvalDrawing.evaluate(); // evaluate drawing after destroying a connection
                } 
            });

            jsPlumb.bind("connectionDrag", function(connection) {
                console.log("connection " + connection.id + " is being dragged. suspendedElement is ", connection.suspendedElement, " of type ", connection.suspendedElementType);
            });

            jsPlumb.bind("connectionDragStop", function(connection) {
                console.log("connection " + connection.id + " was dragged");            
                EvalDrawing.evaluate(); // evaluate drawing after creating a new connection
            });
        });
        
        
});
