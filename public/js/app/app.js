
define([
    'jquery', 
    'underscore', 
    'backbone',
    'models/Drawing',
    'collections/DrawingCollection'
    ], function ($, _, Backbone, Drawing, Drawings) {

        var AppRouter = Backbone.Router.extend({
           routes: {
               // don't know how to use yet
               "api/drawings/:cid" : "getDrawing"
           },   
           initialize: function() {
                console.log('initialized AppRouter');
           },
           getDrawing: function(cid){
               var drawing = Drawings.get(cid);
               return drawing;
           }
        });



        var app = new AppRouter();
        
        Backbone.history.start({pushState: true});    
        

});
