require([
    'jquery',
    'jsplumb',
    'underscore',
    'backbone', 
    'marionette', 
    'collections/StencilBlockCollection',
    'models/StencilBlock',
    'views/StencilView',
    'collections/DrawingCollection',
    'models/Drawing',
    'views/DrawingView',
    'models/DrawingBlock',
    'views/drawingBlockView',
    'app/initializeDrawing',
    'jqueryui'
    ], function ($, jsPlumb, _, Backbone, Marionette, StencilBlocks, StencilBlock, StencilLibraryView, Drawings, Drawing, DrawingView, DrawingBlock, DrawingBlockView,initializeDrawing) {
        
                var libApp = new Marionette.Application();

                libApp.addRegions({
                    'mainRegion': '#StencilLibraryList'
                });

                //$('#tabList a:last').tabs('show'); // setup bootstrap tabs
                
                // Setup library sidebar
                $.get('/api/stencils', function(StencilData) {
    
                    var AccordionView = Backbone.Marionette.CollectionView.extend({
                        //className: 'accordion',
                        id:'StencilList',
                        itemView: StencilLibraryView
                    });
    
                    libApp.addInitializer(function(options){
                       var library = new StencilBlocks(options.library);
                       
                       
                       library.each(function(item){
                             //var stencils  = item.get('stencilBlocks');
                             var stencils = item.get('stencils');
                             var stencilCollection = new StencilBlocks(stencils);
                             item.set('stencils', stencilCollection);
                           /*
                          console.log('libApp.addinitializer item >');
                          console.log(item.get('stencils'));
                          var stencils  = item.get('stencils');
                          var collection = [];
                          for (var s in stencils){
                              var stencil = new StencilBlock(stencils[s]);
                              collection.push(stencil);
                              console.log('stencil is:');
                              console.log(stencil);
                          }
                          console.log(collection);
                          var stencilCollection = new StencilBlocks(collection);
                          item.set('stencils', stencilCollection);
                          console.log('item is now > ');
                          console.log(item);
                           */
                       });
      
                       var accordionView = new AccordionView({
                           collection: library
                       });
       
                       libApp.mainRegion.show(accordionView);
                    });
                    console.log('about to call libApp.start() with the following data:');
                    console.log(StencilData);
                    libApp.start({library:StencilData});
                });
                
                $.get('/api/drawings/Default', function(drawingData) {
                    var editorDrawing = new Drawing(drawingData[0])
                   var editorDrawingView = new DrawingView({model: editorDrawing});
                   $('#editorBoard').append(editorDrawingView.render().el); 
                   window.editorDrawing = editorDrawing; //trying to put this globally
                   //initializeDrawing.init();
               });
                                   

});




