'use strict';


var express = require('express')
    , http = require('http')
    , path = require('path')
    , fs = require('fs')
    , mongoose = require( 'mongoose' ) //MongoDB integration
    , sg = require('./lib/sglib')
    , stencils = sg.stencils()
    ;


var app = express();


// all environments
app.set('port', process.env.PORT || 8080);
app.set('views', __dirname + '/views');

app.set('view engine', 'ejs');

app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.bodyParser());
app.use(express.methodOverride());
app.use(app.router);
app.use(express.static(path.join(__dirname, 'public')));

// development only
if ('development' == app.get('env')) {
  app.use(express.errorHandler());
}



//Connect to database
// Mongoose Connection
mongoose.connect( 'mongodb://localhost/scriptGooey' );

// Setup database schema
var stencilBlockSchema = new mongoose.Schema({
    sgClass: String,
    name: String,
    description: String,
    type: String, //valid types operator, function, source, sink
    value : String,
    editable : Boolean,
    inputs : Number, 
    outputs : Number,
    operation: String
});

var stencilSchema = new mongoose.Schema({
    id: Number,
    name: String,
    stencils: [stencilBlockSchema] 
});

var drawingBlockSchema = new mongoose.Schema({
    cid: String,
    sgClass: String,
    name: String,
    description: String,
    type: String,
    value: String, //TODO: Need to have a system to convert to a number if it is a numeric class
    editable: Boolean,
    inputs: Number,
    outputs: Number,
});

var savedDrawingBlockSchema = new mongoose.Schema({
    cid: String,
    sgClass: String,
    name: String,
    description: String,
    type: String,
    value: String, //TODO: Need to have a system to convert to a number if it is a numeric class
    editable: Boolean,
    inputs: Number,
    outputs: Number,
    left: String, // position x
    top: String   // position y
});

var drawingSchema = new mongoose.Schema({
    name: String
});

var templateSchema = new mongoose.Schema({
   name: String,
   description: String,
   language: String,
   template: String 
});

var connectionSchema = new mongoose.Schema({
    id: String,
    source: String,
    target: String
});

var savedDrawingSchema = new mongoose.Schema({
    name: String,
    created: { type: Date, default: Date.now },
    updated: { type: Date, default: Date.now },
    drawingBlocks: [savedDrawingBlockSchema],
    connections: [connectionSchema]
});

// Session storage
var db_stencilBlocks = mongoose.model('stencilblock', stencilBlockSchema);
var db_stencils = mongoose.model('stencil', stencilSchema);
var db_drawingBlockModel = mongoose.model('drawingblock', drawingBlockSchema);
var db_drawingModel = mongoose.model('Drawing', drawingSchema);
var db_templateModel = mongoose.model('template', templateSchema);

// saved drawings storage
var db_savedDrawingModel = mongoose.model('savedDrawing', savedDrawingSchema);

// populate StencilBlock collection
console.log('Populating StencilBlock collection');

db_stencils.find( function (err, stencilBlocks) {
    if (!err) {
        // console.log('db_stencils did not return an error');
        // console.log('length of db_stencils > '+stencilBlocks.length);
        // console.log( stencilBlocks );
        if ( stencilBlocks.length == 0 ){
            // condition: the database collection is empty
            // now populate
            console.log("Populating ...")
            for (var i in stencils) {
                var stencilCollection = new db_stencils(stencils[i]);
                stencilCollection.save({
                    success: function() {
                        console.log(stencils[i].name + " was saved");
                    },
                    error: function(err){
                        console.error(err);
                    } 
                });
            }
            console.log('done populating');
        } else {
            console.log('Database stencils is already populated');
        }
    } else {
        console.error(err);
    }
});


// populate template collection
console.log('Populating template collection');


//create new drawing within the database

// First check that we already don't have a default drawing in the database
// and if it does not exist, create it. Otherwise don't do anything

var findings = db_drawingModel.find({name: "Default"}, function( err, drawings ) {
    if( !err ) {
        console.log('db_drawingModel did not return an error');
        console.log('length of db_drawingModel > '+drawings.length);
        console.log( drawings );
        if (drawings.length == 0) {
                console.log('Creating new entry for Database');
                var editorDrawing = new db_drawingModel();
                editorDrawing.name = "Default";
                editorDrawing.save({
                    success: function (){
                        console.log("Success!");
                    },
                    error: function(){
                        console.error(err);
                    }
                });
        } else {
            console.log('No need to create new entry');
        }
        
    } else {
        return console.error( err );
    }   
});
// make sure that the drawingblocks are cleared
// TODO: Need to clear the drawingBlocks on refresh
var blocks = db_drawingBlockModel.find( function (err, drawingBlocks) {
    if (!err) {
        console.log('db_drawingBlockModel did not return an error');
        console.log('length of db_drawingBlockModel > '+drawingBlocks.length);
        if (drawingBlocks.length > 0) {
            db_drawingBlockModel.remove( function (err){
                if (err) { console.error(err);}
            });
        }
    } else {
        console.error(err);
    }
});


//// ROUTES ////

// Default Route
app.get('/', function (req, res) {
    res.render('index');
});

// methods to get library icons
app.get('/api/stencils', function (req, res) {
    // Get all stencils from the database
    return db_stencils.find(  function( err, stencils ) {
            if( !err ) {
                console.log('db_stencils did not return an error');
                console.log('length of db_stencils > '+stencils.length);
                return res.send( stencils );
            } else {
                return console.error( err );
            }
        }); 
});

// old methods

app.get('/api/icons', function(req, res){
    // get all stencils for the library
       res.json(stencils);
});

// May not work and does not seem to be required
/*
app.get('/api/stencils/:id/stencil', function  (req, res) {
    // Raw JSON of individual icons
    console.log("req.params.id > "+req.params.id);
    var results = [];
    var i = icons[req.params.id];
    for (var icon in i){
        if (i.hasOwnProperty(icon)) {
            results.push(i[icon]);
        }
    }
    res.json(results);
});
*/

// methods to get and store drawing data
app.get('/api/drawings', function (req, res) {
    // Get all drawings from the database
    return db_drawingModel.find( function( err, drawings ) {
            if( !err ) {
                console.log('db_drawingModel did not return an error');
                console.log('length of db_drawingModel > '+drawings.length);
                return res.send( drawings );
            } else {
                return console.log( err );
            }
        }); 
});

app.get('/api/drawings/:name', function (req, res) {
    // Get specific drawing from the database
    return db_drawingModel.find(req.params.name, function (err, drawing) {
        if (!err) {
            console.log('db_drawingModel with the following name: '+req.params.name+' found')
            return res.send(drawing);
        } else {
            return console.error(err);
        }
    });
    
});

app.post('/api/drawings/', function (req, res) {
    // create drawing with this id in the database
    var newDrawing = new db_drawingModel({
        name: req.body.name
    });
    newDrawing.save( function( err ) {
        if( !err ) {
            return console.log( 'Drawing created' );
        } else {
            return console.log( err );
        }
    });
    return res.send( newDrawing );
});

app.put('/api/drawings/:name', function (req, res) {
   // update drawing in the database
   console.log( 'Updating drawing ' + req.params.name);
       return db_drawingBlockModel.find(req.params.name, function( err, drawingBlock) {
           drawingBlock.value = req.body.value;

           return drawingBlock.save( function( err ) {
               if( !err ) {
                   console.log( 'drawingBlock updated' );
               } else {
                   console.log( err );
               }
               return response.send( drawingBlock );
           });
       });
});

app.get('/api/drawings/:name/drawingblocks', function (req, res) {
   // read drawingblocks associated with this drawing 
   return db_drawingBlockModel.find(req.params.name, function (err, drawingBlocks) {
       if (!err) {
           console.log('db_drawingBlocks found');
           return res.send(drawingBlocks);
       } else {
           return console.error(err);
       }
   });
});

app.post('/api/drawings/:name/drawingblocks', function (req, res) {
   //create drawingblock associated with this drawing 
       var drawingBlock = new db_drawingBlockModel(req.body);
       drawingBlock.save( function( err ) {
           if( !err ) {
               return console.log( 'created new drawingBlock' );
           } else {
               return console.error(err);
           }
       });
      return res.send(drawingBlock);
});

app.put('/api/drawings/:name/drawingblocks/:id', function (req, res) {
    // update values of a specific drawingblock

    console.log( 'Updating drawinBlock ' + req.params.id );
    return db_drawingBlockModel.findByIdAndUpdate(req.params.id, { $set: {value: req.body.value}}, function (err, drawingBlock) {
        if (err) return console.error(err);
        res.send(drawingBlock);
    });
    /*
    return db_drawingBlockModel.findById(req.params.id, function(err, drawingBlock) {
        if (!err) {
            drawingBlock.value = req.body.value;
            drawingBlock.save();
        } else {
            console.error(err);
        }
        res.send(drawingBlock);
     });

*/
});

app.delete('/api/drawings/:name/drawingblocks/:id', function (req, res) {
    // delete a specific drawingblock
    console.log( 'Deleting drawingBlock with id: ' + req.params.id );
        return db_drawingBlockModel.findByIdAndRemove( req.params.id, function (err) {
            if (!err) {
                return res.send('');
            } else {
                return console.error(err);
            }
        });
});
// Save and Load routines
app.get('/api/save/drawings', function (req, res) {
    // get all saved sessions
    return db_savedDrawingModel.find( function (err, drawings) {
        if (!err) {
            return res.send(drawings);
        } else {
            console.error(err);
        }
    });
});

app.post('/api/save/drawings', function (req, res) {
    // create a saved drawing
    console.log("API: save drawings req.body >");
    console.log(req.body);
   //var saveDrawing = new db_savedDrawingModel({drawingBlocks:req.body.drawing});
   var saveDrawing = new db_savedDrawingModel(req.body);
   //saveDrawing.drawing(req.body);
   //saveDrawing.date = new Date;
   saveDrawing.save( function (err, savedDrawing) {
       if (!err) {
           console.log("saved Drawing >");
           console.log(saveDrawing);
       } else {
           console.error(err);
       }

    });
   return res.send(saveDrawing);
});

app.put('/api/save/drawing/:name', function (req, res) {
    // updated a saved drawing by name
    return db_savedDrawingModel.find({name: req.params.name}, function (err, drawing) {
        if (!err) {
            drawing.drawingBlocks = req.body.drawing;
            drawing.updated = new Date;
            drawing.save();
        } else {
            console.error(err);
        }
        res.send(drawing);
    });
});

app.put('/api/save/drawings/:id', function (req, res) {
    // update a saved drawing by id
    return db_savedDawigModel.findById(req.params.id, function (err, drawing) {
        if (!err) {
            drawing.drawingBlocks = req.body.drawing;
            drawing.updated = new Date;
            drawing.save();
        } else {
            console.error(err);
        }
        res.send(drawing);
    });
});



app.get('/api/save/drawings/:name', function (req, res) {
    // retrieve saved drawing by name
    return db_savedDrawingModel.find({name: req.params.name}, function (err, drawing) {
        if (!err) {
            return res.send(drawing);
        } else { 
            console.error(err);
        }
    });
});


app.get('/api/save/drawings/:id', function (req,res) {
    // retrieve saved drawing
    return db_savedDrawingModel.findById( req.params.id, function (err, drawing) {
        if (!err) {
            return res.send(drawing);
        } else {
            console.error(err);
        }
    });
});

        
// template routes
app.get('/api/templates', function (req, res) {
   //get the list of all templates
   return db_templateModel.find( function( err, templates ) {
           if( !err ) {
               console.log('db_templateModel did not return an error');
               console.log('length of db_templateModel > '+templates.length);
               return res.send( templates );
           } else {
               return console.error( err );
           }
       });
   
});
app.get('/api/templates/:name', function (req, res) {
   //get specific template 
   return db_templateModel.find(req.params.name, function (err, template) {
       if (!err) {
           console.log('db_templateModel with the following name: '+req.params.name+' found')
           return res.send(template);
       } else {
           return console.error(err);
       }
   });
});



// setup the server
http.createServer(app).listen(app.get('port'), function(){
  console.log('Express server listening on port ' + app.get('port'));
});
