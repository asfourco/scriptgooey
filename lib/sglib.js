/*  scriptGooey Stencil Libray
    Fadi Asfour, PhD
    August 18, 2013
    
    sglib.js creates a stencil array of icons as defined by icon()

    The output will be in the following format:
    [{
        id:       , 
        name:     ,
        stencils : [ 
                    {
                        sgClass:       , // 'SG_' + name
                        name:          , // name of stencil block
                        description:   , // description of what it does
                        type:          , // valid types are:  function, source, sink,  
                        value:         , // either number or text
                        editable :     , // Boolean to set if the value is changeable
                        inputs :       , // if this icon takes inputs and how many
                        outputs :      , // if this icon gives outputs and how many
                    }
                 ]
    }]
*/

// Define stencil classes
function icon(name, description, type, value, editable, inputs, outputs, operation) {
    this.sgClass = 'SG_'+name;
    this.name = name;
    this.description = description;
    //TODO: create validation routine for type ... 
    // porbably best to this in the model creation routine
    this.type = type; //valid types variable, function, source, sink
    this.value = value;
    this.editable = editable;
    this.inputs = inputs;
    this.outputs = outputs;
    return this;
}

// define collections
// function parameters call (name, description, type, operator, inputs, outputs)
var basic = new Array( 
    new icon("Addition", "Add two numbers", "function","+", false, 2, 1),
    new icon("Subtraction", "Subtract two numbers", "function","-", false, 2, 1),
    new icon("Division", "Divide two numbers", "function","/", false, 2, 1),
    new icon("Multiplication", "Multiply two numbers","function", "*", false, 2, 1),
    new icon("SquareRoot", "Square root of a number", "function", "x^(-1/2)", false, 1, 1),
    new icon("Square", "X to the power of 2", "function", "x^2", false, 1, 1),
    new icon("Cube", "X to the power of 3", "function", "x^3", false, 1, 1),
    new icon("Inverse", "1/X", "function", "1/x", false, 1,1),
    new icon("Log", "Logarithm base 10","function", "log", false, 1,1 ),
    new icon("Ln", "Natural Logarithm","function", "ln", false, 1,1)    
);
// Need to deal with the order of inputs. e.g, for the following which one is x?
// new icon("x^y", "exponent", "function", "x^y", false, "2", "1") 

/* Don't need variables, the connectors are the variables
var variable = new Array(
    new icon("String", "String Constant","variable","path",false, "1", "1"),
    new icon("Number", "Number Constant","variable","value",false, "1","1")
);
*/
var trig = new Array(
    new icon("Cos", "Cosine()", "function", "cos", false, 1,1),
    new icon("Sin", "Sine()", "function", "sin", false, 1,1),
    new icon("Tan", "Tangent()", "function", "tan",false, 1, 1)
);

/* for later 
    // new icon("Cosh", "Hyperbolic Cosine()", "function", "cosh", false, "1","1"),
    // new icon("Sinh", "Hyperbolic Sine()", "function", "sinh", false, "1","1"),
    // new icon("Tanh", "Hyperbolic Tangent()", "function", "tanh",false,  "1", "1")
*/

var src = new Array(
    new icon("Number", "Set a constant value", "source", "value", true, 0, 1),
    new icon("DataSource", "Open a specified URL to data source", "source", "URL", true, 0, 1)
    //new icon("File", "Open a specified file", "source", "path", true, "0","1" ),
    //new icon("CellRange", "Use data from a specific column", "source", "cell range", true, "0","1")
);

var sink = new Array(
    new icon("Number", "Number Constant to be set", "sink", "Result", false, 1, 0),
    new icon("File", "Write to a specified file", "sink", "Target File", true, 1, 0)
    //new icon("CellRange", "Write to a specific column or range", "sink", "Target CellRange", true, "1","0")
);
        
// export icon library default: array
exports.stencils = function stencils() {
        var stencilArray = [
            {id:1, name:"Input", stencils:src},
            {id:2, name:"Output", stencils:sink},
            {id:3, name:"Arithmatic", stencils:basic}, 
            {id:4, name:"Trig", stencils:trig}
        ];
        return stencilArray;
}


/*
exports.stencils = function stencils(category) {
    var stencilArray = [];
    switch (category) {
    case 'basic': 
        stencilArray = basic;
        break;
    // case 'variable':
    //     stencilArray = variable;
    //     break;
    case 'trig':
        stencilArray = trig;
        break;
    case 'src':
        stencilArray = src;
    case 'flat':
        stencilArray = basic.concat(constant).concat(trig).concat(src);;
        break;
    case 'array':
        stencilArray = [
        {id:1, name:"Source", stencils:src},
        {id:2, name:"Sink", stencils:sink},
        {id:3, name:"Arithmatic", stencils:basic}, 
        {id:4, name:"Variables", stencils:variable}, 
        {id:5, name:"Trig", stencils:trig},
        ];
        break;
    };
    return stencilArray;
}
*/
